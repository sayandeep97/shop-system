package com.customer.service.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.customer.service.entity.Customer;
import com.customer.service.repository.CustomerRepository;

@RestController
public class CustomerController {
	
	
	@Autowired
	private CustomerRepository crepo;
	
	@PostMapping("/registerCustomer")
	public String addCustomer(@Valid @RequestBody Customer c)
	{
		crepo.save(c);
		return "customer added successfully";
	}

	@GetMapping("/customer")
	public Customer getCustomerByName(@RequestParam("first_name") String firstName)
	{
		Customer c = crepo.findByFirstName(firstName);
		return c;
	}
	
	@DeleteMapping("/removeCustomer/{id}")
	public String removeCustomer(@PathVariable int id)
	{
		crepo.deleteById(id);
		return "student removed successfully";
	}
}
