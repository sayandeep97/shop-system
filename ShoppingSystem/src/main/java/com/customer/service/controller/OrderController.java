package com.customer.service.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.customer.service.entity.Customer;
import com.customer.service.entity.OrderDetail;
import com.customer.service.entity.Product;
import com.customer.service.repository.CustomerRepository;
import com.customer.service.repository.OrderDetailRepository;
import com.customer.service.repository.ProductRepository;

@RestController
public class OrderController {

	@Autowired
	private OrderDetailRepository orepo;
	
	@Autowired
	private CustomerRepository crepo;
	
	@Autowired
	private ProductRepository prepo;
	
	@PostMapping("/placeOrder")
	public String addOrder(@RequestBody OrderDetail order, @RequestParam("customer_name") String cname, @RequestParam("product_name")String pname)
	{
		Customer customer = crepo.findByFirstName(cname);
		Product product = prepo.findByName(pname);
		order.setCustomer(customer);
		order.setProduct(product);
		orepo.save(order);
		return "Order saved successfully";
	}
	
	@GetMapping("/orders")
	public List <OrderDetail> getAllOrders()
	{
		List <OrderDetail> allOrders = orepo.findAll();
		return allOrders;
	}
	
	@GetMapping("/ordersByCustomerName")
	public ArrayList <Product> getOrdersByName(@RequestParam("first_name") String firstName)
	{
		List <OrderDetail> orders = orepo.findByCustomerFirstName(firstName);
		Iterator <OrderDetail> itr = orders.iterator();
		ArrayList <Product> products = new ArrayList<>();
		while(itr.hasNext())
		{
			OrderDetail order = itr.next();
			Product p = order.getProduct();
			products.add(p);
		}
		return products;
	}
}
