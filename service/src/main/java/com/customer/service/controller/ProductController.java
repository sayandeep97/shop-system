package com.customer.service.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.customer.service.entity.OrderDetail;
import com.customer.service.entity.Product;
import com.customer.service.repository.ProductRepository;

@RestController
public class ProductController {
	
	@Autowired
	private ProductRepository prepo;
	
	@PostMapping("/addProduct")
	public String addProduct(@RequestBody Product p)
	{
		int price = p.getPrice();
		int discount = p.getDiscount();
		int finalPrice = (price - (price * (discount/100)));
		p.setFinalPrice(finalPrice);
		prepo.save(p);
		return "product added successfully";
	}
	
	@GetMapping("/product")
	public Product getProductByName(@RequestParam("name") String name)
	{
		Product p = prepo.findByName(name);
		return p;
	}

	@DeleteMapping("/removeProduct/{id}")
	public String removeProduct(@PathVariable int id)
	{
		prepo.deleteById(id);
		return "product removed successfully";
	}
}
