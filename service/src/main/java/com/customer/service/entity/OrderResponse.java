package com.customer.service.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class OrderResponse {

	private int id;
	
	
	private Date placedAt;
	

	
	
	private String product;


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public Date getPlacedAt() {
		return placedAt;
	}


	public void setPlacedAt(Date placedAt) {
		this.placedAt = placedAt;
	}


	


	public String getProduct() {
		return product;
	}


	public void setProduct(String product) {
		this.product = product;
	}
	
}
